import 'package:first_app/addjob.dart';
import 'package:first_app/services/database.dart';
import 'package:first_app/widget.dart';
import 'package:first_app/widgets/widget.dart';
import 'package:flutter/material.dart';
import 'package:random_string/random_string.dart';

class AdminPage extends StatefulWidget {
  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  final _formKey = GlobalKey<FormState>();
  String companyImage, companyName, jobTitle, jobDescription, jobId;
  DatabaseService databaseService = new DatabaseService();
  bool _isLoading = false;

  createJobOnline() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
      jobId = randomAlphaNumeric(16);
      Map<String, String> jobMap = {
        "jobId": jobId,
        "companyImage": companyImage,
        "companyName": companyName,
        "jobTitle": jobTitle,
        "jobDescription": jobDescription,
      };
      await databaseService.addCmpData(jobMap, jobId).then((value) {
        setState(() {
          _isLoading = false;
          Navigator.pop(context);

          /* Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => AddJob(jobId)));*/
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Admins Only'),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        iconTheme: IconThemeData(color: Colors.black87),
        brightness: Brightness.light,
      ),
      body: _isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Form(
              key: _formKey,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  children: [
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) =>
                          val.isEmpty ? "Enter Image Url" : null,
                      decoration: textFieldInputDecoration("Company Image Url"),
                      onChanged: (val) {
                        companyImage = val;
                      },
                      /*InputDecoration(
                        
                        hintText: "Company Image Url",*/
                    ),
                    /* onChanged: (val) {
                        companyImage = val;
                      },
                    ),*/
                    SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) =>
                          val.isEmpty ? "Enter Company Name" : null,
                      decoration: textFieldInputDecoration("Company Name"),
                      onChanged: (val) {
                        companyName = val;
                      },
                    ),

                    /*InputDecoration(
                        hintText: "Company Name",
                      ),
                      onChanged: (val) {
                        companyName = val;
                      },
                    ),*/
                    SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) =>
                          val.isEmpty ? "Enter Job Title" : null,
                      decoration: textFieldInputDecoration("Job Title"),
                      onChanged: (val) {
                        jobTitle = val;
                      },
                      /*InputDecoration(
                        hintText: "Job Title",
                      ),
                      onChanged: (val) {
                        jobTitle = val;
                      },*/
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      style: simpleTextStyle(),
                      //cursorWidth: double.infinity,
                      validator: (val) =>
                          val.isEmpty ? "Enter Job Description" : null,
                      decoration: textFieldInputDecoration("Job Description"),
                      onChanged: (val) {
                        jobDescription = val;
                      },
                      /*InputDecoration(
                        hintText: "Job Description",
                      ),
                      onChanged: (val) {
                        jobDescription = val;
                      },*/
                    ),
                    Spacer(),
                    GestureDetector(
                        onTap: () {
                          createJobOnline();
                        },
                        child:
                            blueButton(context: context, label: "Create Form")),
                    SizedBox(
                      height: 60,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
