import 'package:first_app/widgets/widget.dart';
import 'package:flutter/material.dart';
import 'package:first_app/services/database.dart';
import 'widget.dart';

class AddJob extends StatefulWidget {
  final String jobId;
  AddJob(this.jobId);

  @override
  _AddJobState createState() => _AddJobState();
}

class _AddJobState extends State<AddJob> {
  final _formKey = GlobalKey<FormState>();
  String name, email, tenth, twelveordip, degreeavg, regisurl;
  bool _isLoading = false;

  DatabaseService databaseService = new DatabaseService();

  uploadJobData() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
      Map<String, String> jobMap = {
        "Name": name,
        "Email": email,
        "10th": tenth,
        "12/dip": twelveordip,
        "DegreeAvg": degreeavg,
        "URL": regisurl,
      };
      await databaseService.addJobData(jobMap, widget.jobId).then((value) {
        setState(() {
          _isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add"),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        iconTheme: IconThemeData(color: Colors.black87),
        brightness: Brightness.light,
      ),
      body: _isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Form(
              key: _formKey,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) => val.isEmpty ? "Enter Name" : null,
                      decoration: textFieldInputDecoration("Name"),
                      onChanged: (val) {
                        name = val;
                      },
                      /*InputDecoration(
                        hintText: "Name",
                      ),
                      */
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) => val.isEmpty ? "Enter email" : null,
                      decoration: textFieldInputDecoration("Email"),
                      /*InputDecoration(
                        hintText: "Email",
                      ),*/
                      onChanged: (val) {
                        email = val;
                      },
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) =>
                          val.isEmpty ? "Enter 10th Marks" : null,
                      decoration: textFieldInputDecoration("10th Marks"),
                      /*InputDecoration(
                        hintText: "1oth Marks",
                      ),*/
                      onChanged: (val) {
                        tenth = val;
                      },
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) =>
                          val.isEmpty ? "Enter 12th/ Dip Marks" : null,
                      decoration: textFieldInputDecoration("12th/Dip Marks"),
                      /*InputDecoration(
                        hintText: "12th/Dip Marks",
                      ),*/
                      onChanged: (val) {
                        twelveordip = val;
                      },
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) =>
                          val.isEmpty ? "Enter Degree Avg" : null,
                      decoration: textFieldInputDecoration("Degree Avg"),
                      /*InputDecoration(
                        hintText: "Degree Avg",
                      ),*/
                      onChanged: (val) {
                        degreeavg = val;
                      },
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      style: simpleTextStyle(),
                      validator: (val) => val.isEmpty ? "Enter URL" : null,
                      decoration: textFieldInputDecoration("URL"),
                      /* InputDecoration(
                        hintText: "URL",
                      ),*/
                      onChanged: (val) {
                        regisurl = val;
                      },
                    ),
                    Spacer(),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: blueButton(
                              context: context,
                              label: "Submit",
                              buttonWidth:
                                  MediaQuery.of(context).size.width / 2 - 36),
                        ),
                        SizedBox(
                          width: 24,
                        ),
                        GestureDetector(
                          onTap: () {
                            uploadJobData();
                          },
                          child: blueButton(
                            context: context,
                            label: "Add",
                            buttonHeight:
                                MediaQuery.of(context).size.height / 20.0,
                            buttonWidth:
                                MediaQuery.of(context).size.width / 2 - 40,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 60,
                    )
                  ],
                ),
              ),
            ),
    );
    // ),
    //);
  }
}
