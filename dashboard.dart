import 'dart:collection';

import 'package:first_app/addstudentdetails.dart';
import 'package:first_app/authenticate.dart';
import 'package:first_app/chatRoomScreen.dart';
import 'package:first_app/services/auth.dart';
import 'package:first_app/student.dart';
import 'package:flutter/material.dart';
import 'package:first_app/profile.dart';
import 'package:first_app/alluserspage.dart';
import 'package:first_app/usermgmt.dart';
import 'package:first_app/myHomePage.dart';
import 'application.dart';
import 'services/crud.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  String role;
  String clgid;

  crudMethods crudObj = new crudMethods();
  Future<bool> addDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Add Data', style: TextStyle(fontSize: 15.0)),
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(hintText: 'Enter role'),
                onChanged: (value) {
                  this.role = value;
                },
              ),
              SizedBox(height: 5.0),
              TextField(
                decoration: InputDecoration(hintText: 'Enter Clg Id'),
                onChanged: (value) {
                  this.clgid = value;
                },
              )
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Add'),
              textColor: Colors.blue,
              onPressed: () {
                Navigator.of(context).pop();
                Map<String, String> userData = {
                  'role': this.role,
                  'ClgId': this.clgid
                };
                crudObj.addData(userData).then((result) {
                  dialogTrigger(context);
                }).catchError((e) {
                  print(e);
                });
                /* signup() async {
                  final formState = _formKey.currentState;
                  if (formState.validate()) {
                    formState.save();
                    try {
                      await FirebaseAuth.instance
                          .createUserWithEmailAndPassword(
                              email: _email, password: _password);
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    } catch (e) {
                      print(e.message);
                    }
                  }
                }*/
              },
            )
          ],
        );
      },
    );
  }

  Future<bool> dialogTrigger(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Job Done', style: TextStyle(fontSize: 15.0)),
            content: Text('Added'),
            actions: <Widget>[
              FlatButton(
                child: Text('Alright'),
                textColor: Colors.blue,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer header'),
              decoration: BoxDecoration(color: Colors.yellow),
            ),
            ListTile(
              title: Text('Applications'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AllUsersPage()),
                );
              },
            ),
            ListTile(
              title: Text('Profile'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Profile()),
                );
              },
            ),
            ListTile(
              title: Text('Admin Page'),
              onTap: () {
                AuthService().authorizeAccess(context);
                //UserManagement().authorizeAccess(context);
                //UserManagement().handleAuth();
              },
            ),
            ListTile(
              title: Text('Logout'),
              onTap: () {
                AuthService().signOut();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => Authenticate()));
              },
            )
          ],
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  addDialog(context);
                }),
            Card(
              margin: EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () {
                  AuthService().authorizeAccess(context);

                  // UserManagement().authorizeAccess(context);
                  //UserManagement().handleAuth();
                },
                splashColor: Colors.teal,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.admin_panel_settings,
                        size: 70.0,
                      ),
                      Text(
                        'Admin Login',
                        style: new TextStyle(fontSize: 17.0),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () {
                  // UserManagement().authorizeAccess1(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            AddStudentDetails()), // MyHomePage()),
                  );
                },
                splashColor: Colors.teal,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      /* IconButton(
                          icon: Icon(Icons.add),
                          onPressed: () {
                            addDialog(context);
                          }),*/
                      Icon(
                        Icons.backpack_sharp,
                        size: 70.0,
                      ),
                      Text(
                        'Student Login',
                        style: new TextStyle(fontSize: 17.0),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            /* Card(
              margin: EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () {
                  String jobId;
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => StudentPage(jobId)),
                  );
                },
                splashColor: Colors.teal,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.work,
                        size: 70.0,
                      ),
                      Text(
                        'Company Login',
                        style: new TextStyle(fontSize: 17.0),
                      ),
                    ],
                  ),
                ),
              ),
            ),*/
            Card(
              margin: EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ChatRoom()));
                },
                splashColor: Colors.teal,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.home,
                        size: 70.0,
                      ),
                      Text(
                        'Chat Room',
                        style: new TextStyle(fontSize: 17.0),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
