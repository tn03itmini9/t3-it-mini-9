// ignore: avoid_web_libraries_in_flutter
//import 'dart:html';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
//import 'main.dart';

void main() {
  runApp(MaterialApp(
    home: Profile(),
  ));
}

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  File sampleImage;

  var imagePicker;

  // get imagePicker => null;

  Future getImage() async {
    var tempImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      sampleImage = tempImage;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          elevation: 1,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
            ),
            onPressed: () {},
          ),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.settings,
                ),
                onPressed: null)
          ],
        ),
        // sampleImage == null ? Text('Select an image') : enableUpload(),

        body: Container(
          padding: EdgeInsets.only(left: 16, top: 25, right: 16),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: ListView(
              children: [
                Text(
                  'Profile',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
                ),
                Center(
                  child: Stack(
                    children: [
                      Container(
                        width: 130,
                        height: 130,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage('ass/shraddha.jpg'),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.white),
                          child: Icon(
                            Icons.edit,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                TextField(
                  decoration: InputDecoration(
                      labelText: 'Full Name',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      hintText: 'Abc Xyz',
                      hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.black)),
                ),
                TextField(
                  decoration: InputDecoration(
                      labelText: 'Email',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      hintText: 'xyz@gmail.com',
                      hintStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                ),
                TextField(
                  decoration: InputDecoration(
                      labelText: 'Contacts',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      //hintText: '1234567899',
                      hintStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                ),
                TextField(
                  decoration: InputDecoration(
                    labelText: 'About',
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: '',
                    hintStyle: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 16, top: 25, right: 16),
                  child: Text(
                    'Upload resume',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
                  ),
                ),
                sampleImage == null ? Text('Select an Image') : enableUpload(),
                new FloatingActionButton(
                  onPressed: getImage,
                  tooltip: 'Add image',
                  child: new Icon(Icons.add),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget enableUpload() {
    return Container(
      child: Column(
        children: <Widget>[
          Image.file(
            sampleImage,
            height: 300.0,
            width: 300.0,
          ),
          RaisedButton(
            elevation: 7.0,
            child: Text('Upload'),
            textColor: Colors.white,
            color: Colors.blue,
            onPressed: () {
              final StorageReference firebaseStorageRef =
                  FirebaseStorage.instance.ref().child('myimg.jpg');
              final StorageUploadTask task =
                  firebaseStorageRef.putFile(sampleImage);
            },
          )
        ],
      ),
    );
  }
}

//import 'dart:html';

//import 'package:first_app/jio.dart';

//corect code
/*import 'package:first_app/menudeatils.dart';
import 'package:flutter/material.dart';
import 'package:first_app/personal_details.dart';

void main() {
  runApp(new MaterialApp(home: new Profile(), routes: <String, WidgetBuilder>{
    "/personal": (BuildContext context) => new Detailss(),
  }));
}

class Profile extends StatefulWidget {
  @override
  _Profile createState() => _Profile();
}

class _Profile extends State<Profile> {
  Widget textfield({@required String hintText, InputDecoration decortion}) {
    return Material(
      elevation: 4,
      shadowColor: Colors.grey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextField(
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(
            letterSpacing: 2,
            color: Colors.black12,
            fontWeight: FontWeight.bold,
          ),
          fillColor: Colors.white30,
          filled: true,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide.none),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.teal,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {},
        ),
      ),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /*Container(
               // child: Padding(
                 // padding: const EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                 // child: Column(
                    //children: [Row(
                      Textfield(
                          decortion: InputDecoration(
                            labelText: "Full Name",
                          ),
                          hintText: null),
                    //],
                  ),
            ],*/
              //),
              //  ),

              /*Container(
                                    margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                                    height: 80.0,
                                    width: 1500.0,
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),*/
              /*child: Padding(
                                      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 20.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: 180.0,
                                                child: Text(
                                                  'Personal Details',
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.white),
                                                ),
                                              ),
                                              Positioned(
                                                top: 5.0,
                                                left: 18.0,
                                                bottom: 5.0,
                                                child: ClipRRect(
                                                    borderRadius: BorderRadius.circular(10.0),
                                                    child: IconButton(
                                                      icon: Icon(Icons.add_box),
                                                      onPressed: () {
                                                        Navigator.pushNamed(context, "/perosnal");
                                                        /*  Navigator.of(context).push(
                                                            MaterialPageRoute(
                                                                builder: (context) => "/personal");*/
                                                      },
                                                    )
                                                    /* Image.asset(
                                                    'ass/jio.png',
                                                    width: 150.0, fit: BoxFit.cover,
                    
                                                    //height: 130.0,
                                                  ),*/
                                                    ),
                                              ),
                                              /* Row(
                                                children: [
                                                  Container(
                                                    child: Text('click '),
                                                  ),
                                                  IconButton(
                                                      icon: Icon(Icons.move_to_inbox),
                                                      onPressed: () {
                                                        Navigator.of(context).push(
                                                            MaterialPageRoute(
                                                                builder: (context) => Detailss()));
                                                      }),
                                                ],
                                              ),*/
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),*/
              /* Container(
                                    margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                                    height: 80.0,
                                    width: 1500.0,
                                    decoration: BoxDecoration(
                                      color: Colors.blueAccent,
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 20.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: 180.0,
                                                child: Text(
                                                  'Qualifications',
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.white),
                                                ),
                                              ),
                                              IconButton(
                                                icon: Icon(Icons.move_to_inbox),
                                                onPressed: () {
                                                  Navigator.of(context)
                                                      .popUntil((route) => route.isFirst);
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) => Detailss()),
                                                  );
                                                },
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),*/
                                 Container(
                                    margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                                    height: 50.0,
                                    width: 1500.0,
                                   child: Textfield(
                    decortion: InputDecoration(
                      border:OutlineInputBorder(),
                      labelText: "Full Name",
                    ),
                                    
                                  /*  decoration: BoxDecoration(
                                      color: Colors.yellowAccent,
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),*/
                         ),),
                                 /* Container(
                                    margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                                    height: 50.0,
                                    width: 1500.0,
                                    /*decoration: BoxDecoration(
                                      color: Colors.greenAccent,
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),*/
                                  ),*/
            ],
          ),
          CustomPaint(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            painter: HeaderCurvedContainer(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text(
                  'Profile',
                  style: TextStyle(
                      fontSize: 35, letterSpacing: 1.5, color: Colors.white),
                ),
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                width: 160, // MediaQuery.of(context).size.width / 2,
                height: MediaQuery.of(context).size.width / 2,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 5),
                  shape: BoxShape.circle,
                  color: Colors.white,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('ass/shraddha.jpg'),
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 300, left: 120),
            child: CircleAvatar(
              backgroundColor: Colors.black54,
              child: IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: Colors.white,
                  ),
                  onPressed: () {}),
            ),
          )
        ],
      ),
    );
  }

  Textfield({InputDecoration decortion, hintText}) {}
}

class HeaderCurvedContainer extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = Colors.tealAccent;
    Path path = Path()
      ..relativeLineTo(0, 150)
      ..quadraticBezierTo(size.width / 2, 225, size.width, 150)
      ..relativeLineTo(0, -150)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}*/
