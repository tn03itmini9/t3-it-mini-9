//page to display students enrolled data ... in admin login

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:first_app/myHomePage.dart';
import 'package:first_app/question_model.dart';
import 'package:first_app/services/database.dart';
import 'package:first_app/widgets/widget.dart';
//import 'package:first_app/widgets/play_company_widget.dart';
import 'package:flutter/material.dart';
import 'package:first_app/widgets/student_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class StudentPage extends StatefulWidget {
  final String jobId;
  StudentPage(this.jobId);
  @override
  _StudentPageState createState() => _StudentPageState();
}

class _StudentPageState extends State<StudentPage> {
  DatabaseService databaseService = new DatabaseService();
  QuerySnapshot questionsnapshot;

  QuestionModel getQuestionModelFromDataSnapshot(
      DocumentSnapshot questionsnapshot) {
    QuestionModel questionModel = new QuestionModel();

    questionModel.name = questionsnapshot.data["Name"];
    questionModel.email = questionsnapshot.data["Email"];
    questionModel.tenth = questionsnapshot.data["10th"];
    questionModel.degree = questionsnapshot.data["DegreeAvg"];
    questionModel.twelve = questionsnapshot.data["12/dip"];

    return questionModel;
  }

  @override
  void initState() {
    print("${widget.jobId}");
    databaseService.getStudentData(widget.jobId).then((value) {
      questionsnapshot = value;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Terna Recruitment App'),
        elevation: 0.0,
        brightness: Brightness.light,
      ),
      body: Container(
        child: Column(
          children: [
            questionsnapshot == null
                ? Container()
                : ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: questionsnapshot.documents.length,
                    itemBuilder: (context, index) {
                      return QuizPlayTile(
                        questionModel: getQuestionModelFromDataSnapshot(
                            questionsnapshot.documents[index]),
                        index: index,
                      );
                    },
                  )
          ],
        ),
      ),
    );
  }
}

class QuizPlayTile extends StatefulWidget {
  final QuestionModel questionModel;
  final int index;
  QuizPlayTile({this.questionModel, this.index});
  @override
  _QuizPlayTileState createState() => _QuizPlayTileState();
}

class _QuizPlayTileState extends State<QuizPlayTile> {
  void customLaunch(command) async {
    if (await canLaunch(command)) {
      await launch(command);
    } else {
      print('cannot launch $command');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("${widget.index + 1} ${widget.questionModel.name}",
              style: simpleTextStyle()
              // TextStyle(fontSize: 17.0, color: Colors.black),
              ),
          OptionTile(
            option: "A",
            email: widget.questionModel.email,
            //  name: widget.questionModel.name,
            tenth: widget.questionModel.tenth,
            twelve: widget.questionModel.twelve,
            degree: widget.questionModel.degree,
          ),
          Row(children: <Widget>[
            IconButton(
                icon: Icon(Icons.email),
                color: Colors.white,
                onPressed: () {
                  customLaunch(
                      'mailto:toyou@gmail.com?subject=test%20subject&body=test%20body');
                })
          ]),
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}
