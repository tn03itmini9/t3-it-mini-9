import 'package:first_app/adminonly.dart';
import 'package:first_app/myHomePage.dart';
import 'package:first_app/widgets/student_widget.dart';
import 'package:flutter/material.dart';

import 'student.dart';

class AdminDashboard extends StatefulWidget {
  @override
  _AdminDashboardState createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Admin Dashboard'),
        centerTitle: true,
      ),
      body: GridView.count(
        padding: EdgeInsets.all(30.0),
        crossAxisCount: 2,
        children: [
          Card(
            margin: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                String cmpId;
                // String studentId;
                String jobId;
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          MyHomePage()), //StudentPage(jobId)),
                );
              },
              splashColor: Colors.teal,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(
                      Icons.headset,
                      size: 70.0,
                    ),
                    Text(
                      'Students Data',
                      style: new TextStyle(
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AdminPage()),
                );
              },
              splashColor: Colors.teal,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(
                      Icons.art_track_sharp,
                      size: 70.0,
                    ),
                    Text(
                      'Add Company',
                      style: new TextStyle(
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
