//import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:first_app/addstudentdetails.dart';
import 'package:first_app/services/database.dart';
import 'package:first_app/widgets/company_widget.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CompanyDetails extends StatefulWidget {
  final String jobId;
  CompanyDetails(this.jobId);

  @override
  _CompanyDetailsState createState() => _CompanyDetailsState();
}

class _CompanyDetailsState extends State<CompanyDetails> {
  DatabaseService databaseService = new DatabaseService();
  QuerySnapshot questionsnapshot;

  QuestionModel getQuestionModelFromDataSnapshot(
      DocumentSnapshot questionsnapshot) {
    QuestionModel questionModel = new QuestionModel();

    questionModel.jobDescription = questionsnapshot.data["jobDescription"];
    questionModel.jobTitle = questionsnapshot.data["jobTitle"];
    questionModel.companyImage = questionsnapshot.data["companyImage"];
    questionModel.companyName = questionsnapshot.data["companyName"];
    questionModel.jobId = questionsnapshot.data["jobId"];

    return questionModel;
  }

  @override
  void initState() {
    print("${widget.jobId}");
    databaseService.getCompany(widget.jobId).then((value) {
      questionsnapshot = value;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Terna Recruitment App'),
        elevation: 0.0,
        brightness: Brightness.light,
      ),
      body: Container(
        child: Column(
          children: [
            questionsnapshot == null
                ? Container()
                : ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: questionsnapshot.documents.length,
                    itemBuilder: (context, index) {
                      return QuizPlayTile(
                        questionModel: getQuestionModelFromDataSnapshot(
                            questionsnapshot.documents[index]),
                        index: index,
                      );
                    },
                  )
          ],
        ),
      ),
    );
  }
}

class QuestionModel {
  String jobTitle, jobDescription, companyName, companyImage, jobId;
}

class QuizPlayTile extends StatefulWidget {
  final QuestionModel questionModel;
  final int index;
  QuizPlayTile({this.questionModel, this.index});
  @override
  _QuizPlayTileState createState() => _QuizPlayTileState();
}

class _QuizPlayTileState extends State<QuizPlayTile> {
  void customLaunch(command) async {
    if (await canLaunch(command)) {
      await launch(command);
    } else {
      print('cannot launch $command');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /*Text(
            "${widget.index + 1} ${widget.questionModel.}",
            style: TextStyle(fontSize: 17.0, color: Colors.black),
          ),*/
          // CompanyTile(uRL: null, email: null, name: null, jobId: null)
          CompanyTilee(
            jobDescription: widget.questionModel.jobDescription,
            jobTitle: widget.questionModel.jobTitle,
            companyImage: widget.questionModel.companyImage,
            companyName: widget.questionModel.companyName,
            jobId: widget.questionModel.jobId,
          ),
          // OptionTile(
          // option: "A",
          //jobDescription: widget.questionModel.jobDescription,
          //  name: widget.questionModel.name,
          //tenth: widget.questionModel.tenth,
          //twelve: widget.questionModel.twelve,
          //degree: widget.questionModel.degree,
          // ),
          /* Row(children: <Widget>[
            IconButton(
                icon: Icon(Icons.email),
                onPressed: () {
                  customLaunch(
                      'mailto:toyou@gmail.com?subject=test%20subject&body=test%20body');
                })
          ]),*/
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}

/* Stream companyStream;
  DatabaseService databaseService = new DatabaseService();

  Widget companyList() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 24),
      child: StreamBuilder(
        stream: companyStream,
        builder: (context, snapshot) {
          return snapshot.data == null
              ? Container()
              : ListView.builder(
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    return CompanyTile(
                      // jobDescription:snapshot.data.documents[index].data["jobDescription"]
                      //uRL: snapshot.data.documents[index].data["companyImage"],
                      //email: snapshot.data.documents[index].data["companyName"],
                      jobDescription:
                          snapshot.data.documents[index].data["jobDescription"],
                      //jobId: snapshot.data.documents[index].data["jobId"],
                    );
                  });
        },
      ),
    );
  }

  @override
  void initState() {
    databaseService.getCompanyData().then((val) {
      setState(() {
        companyStream = val;
      });
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Terna Recruitment App'),
        elevation: 0.0,
        brightness: Brightness.light,
      ),
      body: companyList(),
    );
  }*/
//}

/*class OptionTile extends StatelessWidget {
  final String jobDescription, option;
  OptionTile({@required this.jobDescription, @required this.option});
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Text(
            jobDescription,
            style: TextStyle(fontSize: 17.0, color: Colors.black),
          )
        ],
      ),
    );
  }

  /* @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }*/
}*/

/*class OptionTile extends StatefulWidget {
 // final String option,
  final String jobDescription,option;
  //enth, email, twelve, degree, name, option;
  OptionTile(
      { //@required this.tenth,
    @required this.option,
      //@required this.name,
      // @required this.email,
      //@required this.twelve,
      //@required this.degree,
      @required this.jobDescription,
     // String option});
  Widget build(BuildContext context) {
    var widget;
    return Container(
      child: Row(
        children: [
          /* Text(
                     widget.name,
                     style: TextStyle(fontSize: 17.0, color: Colors.black),
                   ),*/
          /*SizedBox(
                     width: 7.0,
                   ),*/
          Text(
            widget.jobDescription,
            style: TextStyle(fontSize: 17.0, color: Colors.black),
          ),
        ],
      ),
    );
  }

  /* @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }*/
}*/

/*class CompanyTile extends StatelessWidget {
  //final String uRL;
  //final String email;
  //final String name;
  final String jobDescription;
  CompanyTile(
      { //@required this.uRL,
      //@required this.email,
      //@required this.name,
      @required this.jobDescription});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CompanyDetails()

                // AddJob(jobId)
                ));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 8),
        height: 150,
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              /* child: Image.network(
                uRL,
                width: MediaQuery.of(context).size.width - 48,
                fit: BoxFit.cover,
              ),*/
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.black26,
              ),
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  /*Text(
                    email,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.w600),
                  ),*/
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    jobDescription,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w600),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}*/
