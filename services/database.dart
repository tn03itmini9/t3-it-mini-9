import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {
  Future<void> addCmpData(Map cmpData, String cmpId) async {
    await Firestore.instance
        .collection("JobForm")
        .document(cmpId)
        .setData(cmpData)
        .catchError((e) {
      print(e.toString());
    });
  }

  Future<void> addJobData(Map jobData, String cmpId) async {
    await Firestore.instance
        .collection("JobForm")
        .document(cmpId)
        .collection("NewJob")
        .add(jobData)
        .catchError((e) {
      print(e);
    });
  }

  getCompanyData() async {
    return Firestore.instance.collection("JobForm").snapshots();
  }

  getStudentData(String jobId) async {
    return Firestore.instance
        .collection("JobForm")
        .document(jobId)
        .collection("NewJob")
        .getDocuments();
  }

  getCompany(String jobId) async {
    return Firestore.instance.collection("JobForm").document(jobId).snapshots();
  }
}
/*
class DatabaseMethods {
  String get chatRoomId => null;

  getUserInfo(String email) async {
    return await Firestore.instance
        .collection("loggedinUsers")
        .where("email", isEqualTo: email)
        .getDocuments()
        .catchError((e) {
      print(e.toString());
    });
  }

  searchByName(String searchField) {
    return Firestore.instance
        .collection("loggedinUsers")
        .where('name', isEqualTo: searchField)
        .getDocuments();
  }

  Future<void> uploadUserInfo(userDataMap) async {
    Firestore.instance
        .collection("loggedinUsers")
        .add(userDataMap)
        .catchError((e) {
      print(e.toString());
    });
  }

  Future<bool> createChatRoom(charRoomId, chatRoomMap) async {
    Firestore.instance
        .collection("ChatRoom")
        .document(charRoomId)
        .setData(chatRoomMap)
        .catchError((e) {
      print(e.toString());
    });
  }

  /* Future<void> getConversationMessages(
    String chatroomId,
    Map messageMap,
  ) async {
    await Firestore.instance
        .collection("ChatRoom")
        .document(chatroomId)
        //.document(chatRoomMap)
        .collection("chats")
        .add(messageMap)
        .catchError((e) {
      print(e);
    });
  }*/

  getChats(String chatRoomId) async {
    return Firestore.instance
        .collection("chatRoom")
        .document(chatRoomId)
        .collection("chats")
        .orderBy('time')
        .snapshots();
  }

  Future<void> addMessage(String chatRoomId, chatMessageData) async {
    Firestore.instance
        .collection("ChatRoom")
        .document(chatRoomId)
        .collection("chats")
        .add(chatMessageData)
        .catchError((e) {
      print(e.toString());
    });
  }

  /*getConversationMessages(String chatRoomId, Map messageMap) {
    Firestore.instance
        .collection("ChatRoom")
        .document(chatRoomId)
        .collection("chats")
        .add(messageMap)
        .catchError((e) {
      print(e.toString());
    });
  }*/

  /*Future<void> sendMessage(Map chatMessageData, String chatRoomId) async {
    await Firestore.instance
        .collection("ChatRoom")
        .document(chatRoomId)
        .collection("chats")
        .add(chatMessageData)
        .catchError((e) {
      print(e.toString());
    });
  }*/

  getUserChats(String itIsMyName) async {
    return Firestore.instance
        .collection("ChatRoom")
        .where('users', arrayContains: itIsMyName)
        .snapshots();
  }

  // static void sendMessage(String chatRoomId, Map<String, > chatMessageData) {}
}
*/

class DatabaseMethods {
  Future<void> addUserInfo(userData) async {
    Firestore.instance
        .collection("loggedinUsers")
        .add(userData)
        .catchError((e) {
      print(e.toString());
    });
  }

  getUserInfo(String email) async {
    return Firestore.instance
        .collection("loggedinUsers")
        .where("email", isEqualTo: email)
        .getDocuments()
        .catchError((e) {
      print(e.toString());
    });
  }

  searchByName(String searchField) {
    return Firestore.instance
        .collection("loggedinUsers")
        .where('name', isEqualTo: searchField)
        .getDocuments();
  }

  Future<bool> addChatRoom(chatRoom, chatRoomId) {
    Firestore.instance
        .collection("chatRoom")
        .document(chatRoomId)
        .setData(chatRoom)
        .catchError((e) {
      print(e);
    });
  }

  getChats(String chatRoomId) async {
    return Firestore.instance
        .collection("chatRoom")
        .document(chatRoomId)
        .collection("chats")
        .orderBy('time')
        .snapshots();
  }

  Future<void> addMessage(String chatRoomId, chatMessageData) {
    Firestore.instance
        .collection("chatRoom")
        .document(chatRoomId)
        .collection("chats")
        .add(chatMessageData)
        .catchError((e) {
      print(e.toString());
    });
  }

  getUserChats(String itIsMyName) async {
    return await Firestore.instance
        .collection("chatRoom")
        .where('users', arrayContains: itIsMyName)
        .snapshots();
  }
}
