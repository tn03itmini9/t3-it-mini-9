import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class crudMethods {
  bool isLoggedin() {
    if (FirebaseAuth.instance.currentUser() != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> addData(userData) async {
    if (isLoggedin()) {
      Firestore.instance.collection('users').add(userData).catchError((e) {
        print(e);
      });
    }
  }
}
