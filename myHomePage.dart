// ignore: unused_import
//import 'dart:html';

import 'package:first_app/addjob.dart';
import 'package:first_app/menudeatils.dart';
import 'package:first_app/play_company.dart';
import 'package:first_app/services/database.dart';
import 'package:first_app/student.dart';
import 'package:first_app/usermgmt.dart';
import 'package:flutter/material.dart';
import 'package:first_app/jio.dart';
import 'package:first_app/profile.dart';
import 'package:first_app/application.dart';
import 'package:first_app/personal_details.dart';
import 'package:first_app/adminonly.dart';
//import 'package:first_app/post.dart';

class MyHomePage extends StatefulWidget {
  //final String name;
  //MyHomePage(String displayName);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Stream companyStream;
  DatabaseService databaseService = new DatabaseService();

  Widget companyList() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 24),
      child: StreamBuilder(
        stream: companyStream,
        builder: (context, snapshot) {
          return snapshot.data == null
              ? Container()
              : ListView.builder(
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    return CompanyTile(
                      uRL: snapshot.data.documents[index].data["companyImage"],
                      email: snapshot.data.documents[index].data["companyName"],
                      name:
                          snapshot.data.documents[index].data["jobDescription"],
                      jobId: snapshot.data.documents[index].data["jobId"],
                    );
                  });
        },
      ),
    );
  }

  @override
  void initState() {
    databaseService.getCompanyData().then((val) {
      setState(() {
        companyStream = val;
      });
    });
    super.initState();
  }

  /* @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
       // title: appBar(context),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        brightness: Brightness.light,
      ),
    )
  }*/

  /* List<Post> posts=[];
  void newPost (String text){
    var post = new Post(text,widget.name);
 //   savePost(post);
    post.setId( savePost(post)); 
    this.setState((){
      posts.add(post);
    });
  }*/
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer header'),
              decoration: BoxDecoration(color: Colors.yellow),
            ),
            ListTile(
              title: Text('Applications'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Application()),
                );
              },
            ),
            ListTile(
              title: Text('Profile'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Profile()),
                );
              },
            ),
            ListTile(
              title: Text('Logout'),
              /*onTap: () {
                UserManagement().signOut();
                //Navigator.pop(context);
              },*/
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Terna Recruitment App'),
        elevation: 0.0,
        brightness: Brightness.light,
      ),
      body: companyList(),
      /* floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          /* Navigator.push(
              context, MaterialPageRoute(builder: (context) => AdminPage()));
       */
        },
      ),*/
      /*appBar: AppBar(
        elevation: 20.0,
        title: Text('Terna Recruitment App'),
        backgroundColor: Colors.teal,
        actions: [
          CircleAvatar(
            radius: 30.0,
            backgroundImage: AssetImage('ass/bw pb.jpg'),
          ),
        ],
      ),*/
      /* body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset('ass/incubation-450x267.png'),
            SizedBox(
              height: 20.0,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Upcoming Companies',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
              ),
            ),
            Container(),
            FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AdminPage()));
                })
            /*Stack(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                  height: 150.0,
                  width: 1500.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(color: Colors.grey),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(160.0, 10.0, 10.0, 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 120.0,
                              child: Text(
                                'JIO',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Icon(
                              Icons.favorite_border,
                              color: Colors.pink,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Branches Applicable',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic),
                        ),
                        Row(
                          children: [
                            Text('Computer , IT , Elex , Elec'),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text('click here to apply'),
                            ),
                            IconButton(
                              icon: Icon(Icons.move_to_inbox),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Details()),
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 5.0,
                  left: 18.0,
                  bottom: 5.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image.asset(
                      'ass/jio.png',
                      width: 150.0, fit: BoxFit.cover,

                      //height: 130.0,
                    ),
                  ),
                ),
              ],
            ),*/
            /* Stack(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                  height: 150.0,
                  width: 1500.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12.0),
                    boxShadow: [
                      BoxShadow(color: Colors.grey),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(160.0, 10.0, 10.0, 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 120.0,
                              child: Text(
                                'WNS',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Icon(
                              Icons.favorite_border,
                              color: Colors.blueAccent,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Branches Applicable',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic),
                        ),
                        Row(
                          children: [
                            Text('Computer , IT , Elex , Elec'),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text('click here to apply'),
                            ),
                            IconButton(
                              icon: Icon(Icons.move_to_inbox),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Detailss()),
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 5.0,
                  left: 18.0,
                  bottom: 5.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child: Image.asset(
                      'ass/wns.jpg',
                      width: 150.0,
                      fit: BoxFit.cover,
                      //height: 130.0,
                    ),
                  ),
                ),
              ],
            ),
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                  height: 150.0,
                  width: 1500.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12.0),
                    boxShadow: [
                      BoxShadow(color: Colors.grey),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(160.0, 10.0, 10.0, 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 120.0,
                              child: Text(
                                'Ingram Micro',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Icon(
                              Icons.favorite_border,
                              color: Colors.blueAccent,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Branches Applicable',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic),
                        ),
                        Row(
                          children: [
                            Text('Computer , IT , Elex , Elec'),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 5.0,
                  left: 18.0,
                  bottom: 5.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child: Image.asset(
                      'ass/ingram.png',
                      width: 150.0,
                      fit: BoxFit.cover,
                      //height: 130.0,
                    ),
                  ),
                ),
              ],
            ),*/
          ],
        ),
      ),*/
    );

    /* Scaffold(
      appBar: AppBar(title: Text('Hello')),
      body: Column(
        children: <Widget>[
          Expanded(child: PostList(this.posts)),
          TextInputWidget(this.newPost)
        ],
      ),
    );  */
  }

  /*Application() {}*/
}

class CompanyTile extends StatelessWidget {
  final String uRL;
  final String email;
  final String name;
  final String jobId;
  CompanyTile(
      {@required this.uRL,
      @required this.email,
      @required this.name,
      @required this.jobId});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    //   Company(jobId)
                    StudentPage(jobId)
                // AddJob(jobId)
                /* PlayCompany(jobId)*/));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 8),
        height: 150,
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(
                uRL,
                width: MediaQuery.of(context).size.width - 48,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.black26,
              ),
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    email,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    name,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w600),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
