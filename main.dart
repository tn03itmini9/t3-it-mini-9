//import 'dart:html';
/*import 'package:first_app/helperfunc.dart';
import 'package:first_app/login.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:first_app/helperfunc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:first_app/myHomePage.dart';
import 'package:first_app/personal_details.dart';
import 'package:first_app/profile.dart';
import './usermgmt.dart';

import 'package:flutter/material.dart';

final databaseReference = FirebaseDatabase.instance.reference();

void main() {
  runApp(MaterialApp(
    home: MyApp(),
  ));
  //runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool userIsLoggedIn;
  @override
  void initState() {
    super.initState();
  }

  getLoggedInState() async {
    await HelperFunction.getUserLoggedInSharedPreference().then((value) {
      setState(() {
        userIsLoggedIn;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      /*routes: {
       '/': (context) => MyHomePage(),
        '/profile': (context) => Profile(),
        '/personal': (context) => Detailss(),
      },*/
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: UserManagement().handleAuth(),
    );
  }
}*/

//import 'package:chatapp/helper/authenticate.dart';
import 'package:first_app/helperfunc.dart';
//import 'package:chatapp/views/chatrooms.dart';
import 'package:first_app/dashboard.dart';
import 'package:flutter/material.dart';

import 'authenticate.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool userIsLoggedIn;

  @override
  void initState() {
    getLoggedInState();
    super.initState();
  }

  getLoggedInState() async {
    await HelperFunction.getUserLoggedInSharedPreference().then((value) {
      setState(() {
        userIsLoggedIn = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FlutterChat',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xff145C9E),
        scaffoldBackgroundColor: Color(0xff1F1F1F),
        accentColor: Color(0xff007EF4),
        fontFamily: "OverpassRegular",
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: userIsLoggedIn != null
          ? userIsLoggedIn
              ? DashboardPage()
              : Authenticate()
          : Container(
              child: Center(
                child: Authenticate(),
              ),
            ),
    );
  }
}
