import 'package:first_app/widgets/widget.dart';
import 'package:flutter/material.dart';

class OptionTile extends StatefulWidget {
  final String tenth, email, twelve, degree, name, option;
  OptionTile({
    @required this.tenth,
    @required this.option,
    @required this.name,
    @required this.email,
    @required this.twelve,
    @required this.degree,
  });
  @override
  _OptionTileState createState() => _OptionTileState();
}

class _OptionTileState extends State<OptionTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          /* Text(
            widget.name,
            style: TextStyle(fontSize: 17.0, color: Colors.black),
          ),*/
          /*SizedBox(
            width: 7.0,
          ),*/
          Text(widget.email, style: simpleTextStyle()
              // TextStyle(fontSize: 17.0, color: Colors.black),
              ),
          /*  Container(
            decoration: BoxDecoration(),
            child: Text(
              "${widget.tenth}",
              style: TextStyle(color: Colors.black54),
            ),
          ),*/
          SizedBox(
            width: 7.0,
          ),
          Text(widget.tenth,
              //  widget.degree,
              style: simpleTextStyle()
              // TextStyle(fontSize: 17.0, color: Colors.black),
              ),
          SizedBox(
            width: 7.0,
          ),
          Text(widget.twelve, style: simpleTextStyle()
              //TextStyle(fontSize: 17.0, color: Colors.black),
              ),
          SizedBox(
            width: 7.0,
          ),
          Text(widget.degree, style: simpleTextStyle()
              //TextStyle(fontSize: 17.0, color: Colors.black),
              ),
        ],
      ),
    );
  }
}

/*import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:first_app/addjob.dart';
import 'package:first_app/menudeatils.dart';
import 'package:first_app/play_company.dart';
import 'package:first_app/services/database.dart';
import 'package:first_app/usermgmt.dart';
import 'package:flutter/material.dart';
import 'package:first_app/jio.dart';
import 'package:first_app/profile.dart';
import 'package:first_app/application.dart';
import 'package:first_app/personal_details.dart';
import 'package:first_app/adminonly.dart';
//static List<StudentModel> FinalStudentList = [];

class StudentPage extends StatefulWidget {
  final String cmpId;

  StudentPage(this.cmpId);

  @override
  _StudentPageState createState() => _StudentPageState();
}

class _StudentPageState extends State<StudentPage> {
  Stream studentStream;

  DatabaseService databaseService = new DatabaseService();

  Widget studentList() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 24),
      child: FutureBuilder(
        // StreamBuilder(
        // stream: studentStream,
        future: Firestore.instance
            .collection('JobForm/${['cmpId']}/NewJob')
            .getDocuments(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data != null) {
              return StudentTile(
                email: snapshot.data.documents
                    .toList()[0]
                    .data["Email"]
                    .toString(),
                name:
                    snapshot.data.documents.toList()[0].data["Name"].toString(),
              );
            }
          }
          /*return snapshot.data == null
              ? Container()
              : ListView.builder(
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    return StudentTile(
                      // uRL: snapshot.data.documents[index].data["companyImage"],
                      email: snapshot.data.documents[index].data["Email"],
                      name: snapshot.data.documents[index].data["Name"],
                      // jobId: snapshot.data.documents[index].data["jobId"],
                    );
                  });*/
        },
      ),
    );
  }

  /*@override
  void initState() {
    String cmpId;
    databaseService.getStudentData(cmpId).then((val) {
      setState(() {
        studentStream = val;
      });
    });
    super.initState();
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer header'),
              decoration: BoxDecoration(color: Colors.yellow),
            ),
            ListTile(
              title: Text('Applications'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Application()),
                );
              },
            ),
            ListTile(
              title: Text('Profile'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Profile()),
                );
              },
            ),
            ListTile(
              title: Text('Logout'),
              onTap: () {
                UserManagement().signOut();
                //Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Terna Recruitment App'),
        elevation: 0.0,
        brightness: Brightness.light,
      ),
      // body: //studentList(),
    );
  }
}

/*class StudentModel {
  String email;
  String name;

  StudentModel({this.email, this.name});

  factory StudentModel.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;

    return StudentModel(email: data['Email'], name: data['Name']);
  }
}*/

class StudentTile extends StatelessWidget {
  //final String uRL;
  final String email;
  final String name;
  // final String jobId;
  StudentTile({
    //@required this.uRL,
    @required this.email,
    @required this.name,
    // @required this.jobId
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          String jobId;
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddJob(jobId)
                  /* PlayCompany(jobId)*/));
        },
        child: Container(
          margin: EdgeInsets.only(bottom: 8),
          height: 150,
          child: Stack(
            children: [
              /*  ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(
                //  uRL,
                width: MediaQuery.of(context).size.width - 48,
                fit: BoxFit.cover,
              ),
            ),*/
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.black26,
                ),
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      email,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Text(
                      name,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w600),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
    //  );
  }
  // });
}
//}*/
