import 'package:flutter/material.dart';

class CompanyTilee extends StatefulWidget {
  final jobDescription, jobTitle, companyImage, companyName, jobId;
  CompanyTilee(
      {@required this.jobDescription,
      @required this.jobTitle,
      @required this.companyImage,
      @required this.companyName,
      @required this.jobId});
  @override
  _CompanyTileeState createState() => _CompanyTileeState();
}

class _CompanyTileeState extends State<CompanyTilee> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Text(
            widget.jobDescription,
            style: TextStyle(fontSize: 17.0, color: Colors.black),
          ),
          SizedBox(
            width: 7.0,
          ),
          Text(widget.jobTitle),
          SizedBox(
            width: 7.0,
          ),
          Text(widget.companyName),
          SizedBox(
            width: 7.0,
          ),
          SizedBox(
            width: 7.0,
          ),
          Text(widget.jobId),
        ],
      ),
    );
  }
}
